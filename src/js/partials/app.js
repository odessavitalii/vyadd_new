$(window).on('load', function () {
    setTimeout(() => {
        $('body').removeClass('is-preload');
    }, 1000);
});

$('document').ready(function () {
    init();
});

$(window).resize(function () {
    init();
});

function init() {
    const $body = $('body'),
        $window = $(window),
        $header = $body.find('.header'),
        $navLink = $('.header__nav .nav__link'),
        $btnMenu = $('#btnMenu'),
        $btnMenuClose = $('#btnMenuClose'),
        wowHeader = new WOW({
            boxClass: 'wowHeader',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        });
    let pages = $(".page").length,
        scrolling = false,
        curPage = 1,
        $headerHeight = $header.outerHeight();

    function pagination(page, movingUp) {
        scrolling = true;

        let diff = curPage - page,
            oldPage = curPage;

        curPage = page;

        $(".page").removeClass("active small previous");
        $(".page-" + page).addClass("active");
        $(".nav-btn").removeClass("active");
        $(".nav-page" + page).addClass("active");

        if (page > 1) {
            $(".page-" + (page - 1)).addClass("previous");

            if (movingUp) {
                $(".page-" + (page - 1)).hide();

                let hackPage = page;

                setTimeout(() => {
                    $(".page-" + (hackPage - 1)).show();
                }, 600);
            }

            while (--page) {
                $(".page-" + page).addClass("small");
            }
        }

        if (diff > 1) {
            for (let j = page + 1; j < oldPage; j++) {
                $(".page-" + j + " .half").css("transition", "transform .7s ease-out");
            }
        }

        checkInnerPage();
        checkHeaderNavActive();
        checkWow($('.page.active'));

        setTimeout(() => {
            scrolling = false;

            $(".page .half").attr("style", "");

        }, 700);
    }

    function navigateUp() {
        if (curPage > 1) {
            curPage--;
            pagination(curPage, true);
        }
        checkInnerPage();
        checkHeaderNavActive();
    }

    function navigateDown() {
        if (curPage < pages) {
            curPage++;
            pagination(curPage);
        }
        checkInnerPage();
        checkHeaderNavActive();
        checkWow($('.page.active'));
    }

    function checkInnerPage() {
        if (curPage > 1) {
            $body.addClass('inner-page')
        } else {
            $body.removeClass('inner-page')
        }
    }

    function checkHeaderNavActive() {
        $navLink.each((index, item) => {
            if (index + 1 === curPage - 1) {
                $navLink.removeClass('active');
                $(item).addClass('active');
            } else if (!(curPage - 1)) {
                $navLink.removeClass('active');
            }
        })
    }

    function checkWow(pageActive) {
        if (!pageActive.hasClass('page-animated')) {
            const wow = new WOW({
                boxClass: 'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 0,          // distance to the element when triggering the animation (default is 0)
                mobile: true,       // trigger animations on mobile devices (default is true)
                live: true,       // act on asynchronously loaded content (default is true)
                callback: function (box) {
                    // the callback is fired every time an animation is started
                    // the argument that is passed in is the DOM node being animated
                },
                scrollContainer: null // optional scroll container selector, otherwise use window
            });

            pageActive.find('.wow').addClass('wow-removed').removeClass('wow');
            pageActive.find('.wow-removed').removeClass('wow-removed').addClass('wow');
            pageActive.addClass('page-animated');

            wow.init();
        }
    }

    if ($body.hasClass('main-page') && $window.width() > 767) {
        // move to page after mousewheel
        $(document).on("mousewheel DOMMouseScroll", function (e) {
            if (!scrolling) {
                e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0 ? navigateUp() : navigateDown();
            }
        });

        // move to page after click nav btn
        $(document).on("click", ".scroll-btn", function () {
            if (scrolling) return;

            $(this).hasClass("up") ? navigateUp() : navigateDown();
        });

        // move to page after keydown
        $(document).on("keydown", e => {
            if (scrolling) return;

            if (e.which === 38) {
                navigateUp();
            } else if (e.which === 40) {
                navigateDown();
            }
        });

        // move to page after pagination dot
        $(document).on("click", ".nav-btn:not(.active)", function () {
            if (scrolling) return;

            pagination(+$(this).attr("data-target"));
        });

        // move to page 2
        $(document).on("click", '#scrollTo', function (e) {
            e.preventDefault();

            if (scrolling) return;
            pagination(2);
        });

        // move to page after click nav__link in header
        $(document).on("click", ".header__nav .nav__link", function (e) {
            e.preventDefault();

            let index = $(this).index();
            let nextPage = index + 2;

            if (scrolling) return;

            pagination(nextPage);
        })

        wowHeader.init();
        checkWow($('.page.active'));
    }

    if ($body.hasClass('main-page') && $window.width() <= 767 ) {
        $(document).on("click", ".menu-drop .nav__link", function (e) {
            e.preventDefault();

            let id = $(this).attr("href");

            $body.removeClass('is-menu-open');

            $('html,body').animate({
                    scrollTop: $(id).offset().top
                },
                'slow');
        });

        $("[data-mob-scroll]").on('click', function (e) {
            e.preventDefault();

            let id = $(this).data("mob-scroll");

            $('html, body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        });

        $window.on('scroll', function (e) {
            let scrollTop = $(this).scrollTop();

            if (scrollTop) {
                $body.addClass('inner-page')
            } else {
                $body.removeClass('inner-page')
            }
        })
    }

    if ($body.hasClass('page-inner')) {
        $body.css('padding-top', $headerHeight);
    }

    if ($window.width() <= 767) {
        const wow = new WOW({
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function (box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        });

        wowHeader.init();
        wow.init();

        $btnMenu.on('click', function () {
            $body.addClass('is-menu-open');
        });

        $btnMenuClose.on('click', function () {
            $body.removeClass('is-menu-open');
        })
    }
}